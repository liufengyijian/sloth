#!/bin/bash
## 读取配置文件
. ../config/my-config.sh


sed -i "s#@{myhome}#$MYSQL_HOME#g" /etc/my.cnf
sed -i "s#@{mydata}#$MYSQL_DATA#g" /etc/my.cnf
sed -i "s#@{myport}#$MYSQL_PORT#g" /etc/my.cnf
sed -i "s#@{serverid}#1#g" /etc/my.cnf


sed -i "s#@{myhome}#$MYSQL_HOME#g" /etc/init.d/$MYSQL_INID
sed -i "s#@{mydata}#$MYSQL_DATA#g" /etc/init.d/$MYSQL_INID
chkconfig $MYSQL_INID on

##6. 修改用户环境变量，或建立软链接，方便快速调用软件命令；
ln -sf $MYSQL_HOME/bin/mysql /usr/bin

##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh $MYSQL_PORT

##8. 设置监控日志
ln -sf $MYSQL_HOME/data/mysql.err $HOME_LOG
ln -sf $MYSQL_HOME/data/mysql.slow $HOME_LOG

## 启动定时日志清理和备份方案
#cp -f haha.cnf /home/backup/haha
#cp -f haha.cnf /home/backup/haha

## 启动数据库，并执行初始化脚本
service mysqld start
mysql -u root -p --auto-rehash --line-numbers --show-warnings mysql < default-init.sql

