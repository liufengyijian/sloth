#!/bin/bash
set -e
## 检查输入参数
if [ $# -lt 1 ]; then
    echo "$0 错误：请传入参数，(1、序号; 2、端口号:8181)！"
	echo "例如：./$0 1 8181 或 ./$0 2 8282"
	exit 2
fi
## 读取配置文件
. ../config/my-config.sh

if [ ! -f "$TOMCAT_FILE" ]
then
##1.下载安装包，并上传到服务器；
	TOMCAT_URL=$URL_FILE/tomcat
	wget $TOMCAT_URL/$TOMCAT_FILE
fi
TOMCAT_PORT=$2
NEW_HOME=$TOMCAT_HOME/tomcat-${TOMCAT_PORT}
##2. 解压安装包到指定目录，修改文件的读写执行权限；
mkdir -p $NEW_HOME
tar -zxvf $TOMCAT_FILE -C $NEW_HOME

read -p "增加管理平台……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	## 备份应用目录，并清空tomcat自带的应用
	cp -rf $NEW_HOME/webapps $BACKUP_DIR
	rm -rf $NEW_HOME/webapps/* || true
	cp -f tomcat-users.xml $NEW_HOME/conf
else 
	echo '					跳过'
fi

##4. 修改软件的相关配置文件，本地化或优化配置；
cp -f balance-server.xml $NEW_HOME/conf/server.xml
sed -i "s#@{num}#${1}#g" $NEW_HOME/conf/server.xml

cp -f catalina.sh $NEW_HOME/bin
sed -i "s@\${JAVA_OPTS}@${JAVA_OPTS}@g" $NEW_HOME/bin/catalina.sh

NEW_INID=tomcat7-${TOMCAT_PORT}
##6. 把软件注册为服务，并随系统启动；
cp -f tomcat7 /etc/init.d/$NEW_INID
chmod a+x /etc/init.d/$NEW_INID
## 先替换服务文件中的一些内容
sed -i "s#@{port}#$TOMCAT_PORT#g" /etc/init.d/$NEW_INID
sed -i "s#@{tomcat}#$NEW_HOME#g" /etc/init.d/$NEW_INID
sed -i "s#@{jdk}#$JAVA_HOME#g" /etc/init.d/$NEW_INID
sed -i "s@\${JAVA_OPTS}@${JAVA_OPTS}@g" /etc/init.d/$NEW_INID
chkconfig $NEW_INID on

##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh $TOMCAT_PORT

##8. 设置监控日志
ln -sf $NEW_HOME/logs/catalina.out $HOME_LOG/catalina.out.${TOMCAT_PORT}