#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh

if [ ! -f "$TOMCAT_FILE" ]
then
##1.下载安装包，并上传到服务器；
TOMCAT_URL=$URL_FILE/tomcat
wget $TOMCAT_URL/$TOMCAT_FILE

fi
##2. 解压安装包到指定目录，修改文件的读写执行权限；
mkdir -p /home/tomcat
tar -zxvf $TOMCAT_FILE -C /home/tomcat
mv -f /home/tomcat/$TOMCAT_VERSIOIN $TOMCAT_HOME

##4. 修改软件的相关配置文件，本地化或优化配置；
cp -f $TOMCAT_CNF $TOMCAT_HOME/conf/server.xml

# 复制 管理员文件
# cp -f tomcat-users.xml $TOMCAT_HOME/conf/tomcat-users.xml

##6. 把软件注册为服务，并随系统启动；
cp -f $TOMCAT_INID /etc/init.d/$TOMCAT_INID
chmod a+x /etc/init.d/$TOMCAT_INID
## 先替换服务文件中的一些内容
sed -i "s#@{tomcat}#$TOMCAT_HOME#g" /etc/init.d/$TOMCAT_INID
sed -i "s#@{jdk}#$JAVA_HOME#g" /etc/init.d/$TOMCAT_INID
chkconfig $TOMCAT_INID on

##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh $TOMCAT_PORT

##8. 设置监控日志
ln -sf $TOMCAT_HOME/logs/catalina.out $HOME_LOG/catalina.out 

##9. 定时清理任务
echo 请编写定时清理任务脚本！

##10. 定时备份任务
echo 请编写定时备份脚本！

##能够安全干净删除已安装软件，并重新安装
echo 请编写卸载脚本！