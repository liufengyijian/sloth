#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh

## 重新设置下载链接
JDK_URL=$URL_FILE/java
mkdir -p "$JAVA_HOME"

## 如果文件不存在，则下载
if [ ! -f "$JDK_FILE" ]
then
    wget $JDK_URL/$JDK_FILE
    ## tar -zxvf 解压并列出详情; -C 指定目录
	tar -zxvf $JDK_FILE -C /home/java
fi

##写入环境变量 
JAVA_PROFILE=$SYS_PROFILE_DIR/java.sh

## 单句输入，转义字符"\"
# echo "" > $JAVA_PROFILE
# echo "JAVA_HOME=$JAVA_HOME" >> $JAVA_PROFILE
# echo "CLASSPATH=.:\$JAVA_HOME/lib/dt.jar:\$JAVA_HOME/lib/tools.jar" >> $JAVA_PROFILE
# echo "PATH=$PATH:\$JAVA_HOME/bin" >> $JAVA_PROFILE
# echo "export JAVA_HOME CLASSPATH PATH" >> $JAVA_PROFILE

## 块输入，转义字符"\"
cat > $JAVA_PROFILE <<END
JAVA_HOME=$JAVA_HOME
CLASSPATH=.:\$JAVA_HOME/lib/dt.jar:\$JAVA_HOME/lib/tools.jar
PATH=\$PATH:\$JAVA_HOME/bin
export JAVA_HOME CLASSPATH PATH
END

##修改文件权限
chmod 755 $JAVA_PROFILE
##刷新环境变量
source /etc/profile
##查看是否安装成功
java -version