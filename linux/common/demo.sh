#!/bin/bash
##0. 环境检查，查看系统版本，存储环境，内存使用，变量设置等等
cat /proc/meminfo 查看内 存信息 
cat /proc/cpuinfo （Linux查看cpu相关信息，包括型号、主频、内核信息等）
cat /etc/issue 查看系统版本
env # 查看环境变量资源 
df -h # 查看各分区使用情况 

## 查看已安装软件
rpm -qa | grep mysql >> /home/logs/installed.log
rpm -qa | grep jdk >> /home/logs/installed.log
rpm -qa | grep http >> /home/logs/installed.log

## 读取配置文件
. ../config/my-config.sh

##1.下载安装包，并上传到服务器；
wget $URL_FILE/$FILE

##2. 解压安装包到指定目录，修改文件的读写执行权限；
mkdir -p /home
tar -zxvf $FILE -C /home

##3. 编译源文件，并进行安装；
./configure --prefix=/home/local
make
make install

##4. 修改软件的相关配置文件，本地化或优化配置；
cp -f $CNF /cnf/$new_conf

##5. 修改用户环境变量，或建立软链接，方便快速调用软件命令；
ln -sf /home/bin/haha /usr/bin

##6. 把软件注册为服务，并随系统启动；
cp -f $SERVICED /etc/init.d/$SERVICED
chmod a+x /etc/init.d/$SERVICED
chkconfig $SERVICED on

##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh $PORT

##8. 设置监控日志
#在全局日志目录中创建该软件的日志文件快捷键，方便查看，注两个参数都是绝对路径
ln -sf /home/app/my.log $HOME_LOG/$LOG 

##9. 定时清理任务

##10. 定时备份任务
cp -f haha.cnf /home/backup/haha

cat > aa  << END
##能够安全干净删除已安装软件，并重新安装
echo 请编写卸载脚本！
END
