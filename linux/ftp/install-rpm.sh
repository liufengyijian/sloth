#!/bin/bash
set -e
## 检查输入参数
if [ $# -lt 2 ]; then
    echo "$0 错误！！！请传入参数：1、主端口； 2、最小端口；3、最大端口"
	echo "例如：./$0 40000 40001 40080"
	exit 2
fi
## 读取配置文件
. ../config/my-config.sh

if [ ! -f "$FTP_FILE" ]
then
##1.下载安装包，并上传到服务器；
FTP_URL=$URL_FILE/ftp
wget $FTP_URL/$FTP_FILE
fi

##2. rpm安装
rpm -ivh $FTP_FILE

##4. 修改软件的相关配置文件，本地化或优化配置；
cp -f /etc/vsftpd/vsftpd.conf $BACKUP_DIR
cp -f vsftpd.conf.simple /etc/vsftpd/vsftpd.conf
sed -i "s#@{mainPort}#$1#g" /etc/vsftpd/vsftpd.conf
sed -i "s#@{minPort}#$2#g" /etc/vsftpd/vsftpd.conf
sed -i "s#@{maxPort}#$3#g" /etc/vsftpd/vsftpd.conf
## 允许切换到用户宿主目录
setsebool -P ftp_home_dir  on
setsebool -P allow_ftpd_full_access on


##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh $1
../common/change-iptables.sh ${2}:${3}

## 设置开机启动
chkconfig vsftpd on
service vsftpd restart
