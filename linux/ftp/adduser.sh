#!/bin/bash
set -e
## 检查输入参数
if [ $# -lt 3 ]; then
    echo "$0 错误：请传入参数，1、用户名； 2、密码；3、主目录；4、是否修改为主目录拥有者y/n！"
	echo "例如：./$0 user1 123456 /home/install n"
	exit 2
fi

## 判断用户是否存在
if  grep -q "$1" /etc/vsftpd/chroot_list
then 
	echo "$1 is already exists!"
else 
	## 增加用户
	adduser -d $3 -g ftp -s /sbin/nologin $1
	echo $2 | passwd --stdin $1
	## 限制用户只能访问主目录
	echo $1 >> /etc/vsftpd/chroot_list
fi

## 修改主目录拥有者，使其拥有增删改查权限
if [ "$4" == "y" ] || ["$4" == "Y"];then
	chown $1 $3
else
	echo "$1 为只读用户！"
fi