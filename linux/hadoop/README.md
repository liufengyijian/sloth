### 参考资料和说明
**参考资料：** [hadoop-2.3.0详细安装过程](http://blog.itpub.net/29734436/viewspace-1258972/)
**码云GIT：** [hadoop-2.4.0家族自动化安装脚本](https://git.oschina.net/lucky110100/sloth/tree/master/linux/hadoop?dir=1&filepath=linux%2Fhadoop&oid=71c891520b4e8094f2803f33b02b3544b6480096&sha=5eeef56f32252c5abf0bae75d86813d31bd1a990) 》》》 [获取邀请码](http://git.oschina.net/signup?inviter=lucky110100)   
**版本信息：** hadoop-2.4.0-x64、hive-1.2.1、Zookeeper-3.4.5、Hbase-0.98.21
你没看错，这就是2.4.0版本的安装脚本，虽然参考的是2.3.0的。  

---

### 安装环境
**虚拟机软件：** VirtualBox-4.3.8-92456-Win.exe  
**虚拟机系统：** CentOS-6.5-x86_64-bin-DVD1.iso，64位，分配1CPU，2G内存  
**Hadoop版本：** hadoop-2.4.0-64bit.tar.gz [下载](http://www.aboutyun.com/thread-7688-1-1.html)  
**Nginx文件服务器：** nginx-1.10.0.zip Windows版本，解压即用，主要用来提供文件下载。

主要说一下这些脚本都是干什么的，具体安装过程请到git中查看脚本内容。  
### 执行脚本
**sloth/linux/hadoop/sln-2.4.0.sh**
hadoop自动安装的主要入口，以root用户执行，操作示例如下：  

```
Last login: Wed Sep 28 15:49:27 2016 from 172.16.4.102
[root@name00 ~]# cd /home/install/hadoop/
[root@name00 hadoop]# ./sln-2.4.0.sh 
./sln-2.4.0.sh 错误：请传入参数，(1、类型:name/node;2、机器名：name00/node01;3、机器IP)！
例如：./sln-2.4.0.sh name name00 127.0.0.1
[root@name00 hadoop]# ./sln-2.4.0.sh node node04 172.16.117.4
参数确认： 类型：node   机器名： node04    机器IP：172.16.117.4
继续/跳过(Y/N):y
                                        开始安装……
SSH链接慢？(Y/N):
                                        跳过……
授权公钥……继续/跳过(Y/N):
                                        跳过……
下载脚本……继续/跳过(Y/N):
                                        跳过……
删除已rpm安装的软件……继续/跳过(Y/N):
                                        跳过……
安装java……继续/跳过(Y/N):
                                        跳过……
安装mysql……继续/跳过(Y/N):
                                        跳过……
安装hadoop……继续/跳过(Y/N):
                                        跳过……
安装hive……继续/跳过(Y/N):
                                        跳过……
安装zookeeper……继续/跳过(Y/N):
                                        跳过……
安装hbase……继续/跳过(Y/N):
                                        跳过……
设置环境变量……继续/跳过(Y/N):
                                        跳过……
立即加入Hadoop集群……继续/跳过(Y/N):
                                        跳过……
[root@name00 hadoop]# 
```

允许用户交互，根据需要选择安装。

---
### 测试和监控

```
# 切换用户  
su hadoop  
# 启动所有  
cd /home/hadoop/src/hadoop-2.4.0/sbin/;./start-all.sh  
# 停止所有  
cd /home/hadoop/src/hadoop-2.4.0/sbin/;./stop-all.sh
```

##### 验证  

```
hadoop fs -ls hdfs://name00:9000 /
hadoop fs -mkdir hdfs://name00:9000 /testfolder
```


##### mapreduce例子  

```
cd /home/hadoop/src/hadoop-2.4.0/share/hadoop/mapreduce/
hadoop fs -copyFromLocal /home/logs/hive/hive.log /input/hive.log
hadoop fs -ls /input
hadoop jar hadoop-mapreduce-examples-2.4.0.jar wordcount /input/hive.log /wordout
hadoop fs -ls /wordout
hadoop fs -tail /wordout/part-r-00000
```

##### web监控
> 服务器状态  
> http://name00:50070/dfshealth.html  
> 集群信息  
> http://name00:8088/cluster  
> 查看所有日志信息  
> http://name00:50070/logs/  


---

### 问题或期望：
##### 1. Hadoop所有节点两两间无秘访问  
hadoop需要在各个节点创建一个用户，并保证用户之间能够无秘访问，互联互通，虽然现在能够用脚本自动生成公钥，并将公钥分发给各个节点，但是两两节点间第一次登录访问还是需要确认和输入密码，节点少当然没问题，但是节点多了，一个个去确认也麻烦，求解决方案！！！
##### 2. 下载更新脚本  
目前的实现方式如下  

```
read -p "下载脚本……继续/跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	##下载脚本，并删除已rpm安装的软件
	ssh root@$3 "mkdir -p /home/install"
	## 下载脚本
	#ssh root@$3 "cd /home/install;wget -N -r -nH -np -k -L -p $URL_SHELL" #层级下载有点问题
	## 下载脚本包
	ssh root@$3 "cd /home/install;rm -f linux.zip;wget $URL_SHELL/linux.zip"
	## 解压并提权
	ssh root@$3 "cd /home/install;unzip -o linux.zip;chmod a+x /home/install -R"
else 
	echo '					跳过……'
fi
```
需要先在windows下打包一个所有脚本的zip包，再通过wget命令下载，我希望直接从文件服务器遍历下载更新过的脚本，并覆盖，这样就不用每次更新一个脚本都要手动打包一次。  
原来我尝试过一次

```
## 下载脚本
ssh root@$3 "cd /home/install;wget -N -r -nH -np -k -L -p $URL_SHELL"
```
但是这个命令本地执行没问题，但远端执行就返回错误，会中断整个安装脚本，所以才改成下载压缩包的。