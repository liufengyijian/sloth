#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh

echo '查看zookeeper集群信息'
if [ -f 'zk.info'  ];then
	chmod a+x zk.info
	cat zk.info
else
	touch zk.info
fi

read -p "是否继续(Y/N)？：" isY
if [ "${isY}" != "y" ] && [ "${isY}" != "Y" ];then
   exit 1
fi

## 生成集群配置文件 $ZOO_HOME/conf/zoo.cfg
cat > zoo.cfg <<END
tickTime=2000
initLimit=10
syncLimit=5
dataDir=$ZOO_DATA
dataLogDir=$ZOO_LOGS
clientPort=2181
END

## 生成临时脚本文件
rm -f tmp.sh
echo "set -e" > tmp.sh

## 生成循环安装脚本
rm -f loop.sh
echo "set -e" > loop.sh

echo "循环写入文件"
i=0
while read line
do 
    eval $(echo $line | awk '{print "i1="$1";i2="$2";i3="$3}')
    echo "server.${i}=${i2}:2888:3888" >> zoo.cfg
    ## 写入临时脚本
	echo "ssh root@$i3 'echo ${i} > $ZOO_DATA/myid'" >> tmp.sh
	echo "ssh root@$i3 'chown -R hadoop:hadoop $ZOO_HOME'" >> tmp.sh
	echo "ssh root@$i3 'chown -R hadoop:hadoop $ZOO_DATA'" >> tmp.sh
	echo "ssh root@$i3 'chown -R hadoop:hadoop $ZOO_LOGS'" >> tmp.sh
    i=$((i+1))
done < zk.info

echo "分发文件到各个节点"
while read line
do 
    eval $(echo $line | awk '{print "i1="$1";i2="$2";i3="$3}')
    echo "$i2"
    scp zoo.cfg root@${i3}:$ZOO_HOME/conf
done < zk.info

echo "执行临时脚本"
chmod a+x tmp.sh;./tmp.sh
