#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh

## 重新设置下载链接
HADOOP_URL=$URL_FILE/hadoop
mkdir -p "$HADOOP_HOME"

## 如果文件不存在，则下载
if [ ! -f "$HADOOP_FILE" ]
then
    wget $HADOOP_URL/$HADOOP_FILE
    ## tar -zxvf 解压并列出详情; -C 指定目录
	## tar -zxvf $HADOOP_FILE -C /home/soft
fi
rm -rf /home/soft/$HADOOP_VERSION
tar -zxvf $HADOOP_FILE -C /home/soft
##写入环境变量 
HADOOP_PROFILE=$SYS_PROFILE_DIR/hadoop.sh

## 块输入，转义字符"\"
cat > $HADOOP_PROFILE <<END
HADOOP_HOME=$HADOOP_HOME
PATH=/usr/lib64/qt-3.3/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PATH=\$PATH:\$HADOOP_HOME/bin
export HADOOP_COMMON_LIB_NATIVE_DIR=${HADOOP_HOME}/lib/native  
export HADOOP_OPTS="-Djava.library.path=${HADOOP_HOME}/lib" 
export HADOOP_HOME PATH
END

##修改文件权限
chmod 755 $HADOOP_PROFILE
##刷新环境变量
source /etc/profile
## 创建文件夹
mkdir -p /home/soft/data
mkdir -p /home/soft/name
mkdir -p /home/soft/temple
mkdir -p /home/soft/temp

## 建立Hadoop 用户并赋权
if !(egrep "^hadoop" /etc/passwd >& /dev/null)
then 
	groupadd hadoop
	useradd hadoop -g hadoop
	echo '123456' | passwd --stdin hadoop
fi
chown -R hadoop:hadoop /home/soft

## 无秘钥ssh
cat <<end
su hadoop
cd /home/hadoop/
ssh-keygen -q -t rsa -N "" -f /home/hadoop/.ssh/id_rsa
cd .ssh
cat id_rsa.pub > authorized_keys
chmod go-wx authorized_keys

cd /home/soft/hadoop-2.2.0/etc/hadoop/
vi hadoop-env.sh
vi yarn-env.sh
core-site.xml
hdfs-site.xml
mapred-site.xml
yarn-site.xml
capacity-scheduler.xml

hadoop namenode -format
cd /home/soft/hadoop-2.2.0/sbin
./start-dfs.sh
end