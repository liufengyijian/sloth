#!/bin/bash
set -e

## 检查输入参数
if [ $# -lt 1 ]; then
    echo "$0 错误：请传入参数，(机器名：name00/node01)！"
	echo "例如：$0 name00"
	exit 2
fi
## 读取配置文件
#. ../config/my-config.sh

mv /home/data /home/data2 || true
mkdir -p /home/data
uid=$(id -u hadoop)
gid=$(id -g hadoop)
#umount /home/data||true
cp -f /etc/fstab /home/backup
sed -i "/$1/d" hadoop.info
#echo "//172.16.1.117/hadoop/$1   /home/data cifs defaults,auto,username=user,password=Lutong.hh12,rw,uid=$uid,gid=$gid 0 0" >> /etc/fstab
echo "//172.16.1.119/hadoop/$1   /home/data cifs defaults,auto,username=user,password=lutong<119>,rw,uid=$uid,gid=$gid 0 0" >> /etc/fstab
mount -a
cp -rf /home/data2/* /home/data


#myname=node11901;scp mount-single.sh root@$myname:/home/install/hadoop;
#ssh root@$myname "cd /home/install/hadoop;chmod a+x mount-single.sh;./mount-single.sh $myname"