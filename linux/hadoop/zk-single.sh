#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh
## 重新设置下载链接
ZOO_URL=$URL_FILE/hadoop
mkdir -p $ZOO_HOME

## 如果文件不存在，则下载
if [ ! -f "$ZOO_FILE" ]
then
    wget $ZOO_URL/$ZOO_FILE
fi
rm -rf $ZOO_HOME
tar -zxvf $ZOO_FILE -C ${HADOOP_DIR_OWN}/src

## 创建并重新设置目录的拥有者
mkdir -p $ZOO_DATA
mkdir -p $ZOO_LOGS
chown -R hadoop:hadoop $ZOO_DATA
chown -R hadoop:hadoop $ZOO_LOGS
