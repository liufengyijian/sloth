#!/bin/bash
set -e
#######################	hive 	#######################
## 读取配置文件
. ../config/my-config.sh
## 重新设置下载链接
HIVE_URL=$URL_FILE/hadoop
mkdir -p $HIVE_HOME

## 如果文件不存在，则下载
if [ ! -f "$HIVE_FILE" ]
then
    wget $HIVE_URL/$HIVE_FILE
fi
rm -rf $HIVE_HOME
tar -zxvf $HIVE_FILE -C /home/hadoop/src
##写入环境变量 
HIVE_PROFILE=$SYS_PROFILE_DIR/hive.sh

## 设置配置文件
cp $HIVE_VERSION/hive-env.sh $HIVE_HOME/conf
cp $HIVE_VERSION/hive-site.xml $HIVE_HOME/conf
cp $HIVE_VERSION/hive-log4j.properties $HIVE_HOME/conf
cp mysql-connector-java-5.1.21-bin.jar $HIVE_HOME/lib
## 替换变量
sed -i "s@# HADOOP_HOME=\${bin}/../../hadoop@HADOOP_HOME=${HADOOP_HOME}@g" $HIVE_HOME/conf/hive-env.sh
sed -i "s@# export HIVE_CONF_DIR=@export HIVE_CONF_DIR=$HIVE_HOME/conf@g" $HIVE_HOME/conf/hive-env.sh
#sed -i "s@# export HIVE_AUX_JARS_PATH=@export HIVE_AUX_JARS_PATH==$HIVE_HOME/lib@g" $HIVE_HOME/conf/hive-env.sh

#初始化数据库
mysql -u root --auto-rehash --line-numbers --show-warnings mysql -e "create database if not exists hive character set = 'utf8' collate = 'utf8_general_ci';"

sed -i "s@\${jdbcURL}@jdbc:mysql://${HADOOP_MASTER_IP}:${MYSQL_PORT}/hive?useUnicode=true\&amp;characterEncoding=utf-8@g" $HIVE_HOME/conf/hive-site.xml
sed -i "s@\${jdbcDriver}@com.mysql.jdbc.Driver@g" $HIVE_HOME/conf/hive-site.xml
sed -i "s@\${dbUser}@${dbUser1}@g" $HIVE_HOME/conf/hive-site.xml
sed -i "s@\${dbPwd}@${dbPwd1}@g" $HIVE_HOME/conf/hive-site.xml
mkdir -p $HIVE_LOGS
sed -i "s@\${hiveLog}@$HIVE_LOGS@g" $HIVE_HOME/conf/hive-log4j.properties

## 替换包
mv $HADOOP_HOME/share/hadoop/yarn/lib/jline-0.9.94.jar /home/backup || true
cp -f $HIVE_HOME/lib/jline-2.12.jar $HADOOP_HOME/share/hadoop/yarn/lib

## 重新设置目录的拥有者
chown -R hadoop:hadoop /home/hadoop
chown -R hadoop:hadoop /home/logs/hive
