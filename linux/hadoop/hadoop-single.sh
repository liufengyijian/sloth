#!/bin/bash
set -e
## 检查输入参数
if [ $# -lt 3 ]; then
    echo "$0 错误：请传入参数，(1、类型:name/node;2、机器名：name00/node01;3、机器IP)！"
	echo "例如：$0 name name00 127.0.0.1"
	exit 2
fi

## 读取配置文件
. ../config/my-config.sh
##################### 第一部分 初始化安装环境 ########################
echo "准备安装机器信息》》主机名：$HADOOP_MASTER    类型：$1   机器名： $2    机器IP：$3"

# 关闭一些安全限制
##关闭iptables：
service iptables stop
chkconfig iptables off
##关闭selinux： 
setenforce 0
sed -i "s@^SELINUX=enforcing@SELINUX=disabled@g" /etc/sysconfig/selinux

##查看hostname
sed -i "/HOSTNAME/d" /etc/sysconfig/network
echo "HOSTNAME=$2" >> /etc/sysconfig/network
hostname $2

# 时间同步
if [ "$1" == "name" ];then
	service ntpd stop
	sed -i "/$3/d" /etc/ntp.conf
	echo "server 127.127.1.0" >> /etc/ntp.conf
	echo "fudge 127.127.1.0 stratum 10" >> /etc/ntp.conf
	service ntpd restart
elif [ "$1" == "node" ];then
	#eval $(grep "$HADOOP_MASTER" hadoop.info | awk '{print "tempVal="$1}');
	#同步主机时间，同步失败也返回true，保证下面的步骤继续进行
	ntpdate $HADOOP_MASTER_IP || true
else 
	echo '跳过时间同步………………………………'
fi

echo "创建hadoop用户以及相关目录"
if !(egrep "^hadoop" /etc/passwd >& /dev/null)
then  
	groupadd hadoop
	useradd hadoop -g hadoop
	echo '123456' | passwd --stdin hadoop
fi

echo "创建hadoop相关安装目录"
# 创建hadoop相关安装目录
mkdir -p ${HADOOP_DIR_OWN}/src
mkdir -p ${HADOOP_DIR_OWN}/tools
mkdir -p ${HADOOP_DIR_OWN}/logs
chown -R hadoop.hadoop ${HADOOP_DIR_OWN}
chown -R hadoop.hadoop ${HADOOP_DIR_OWN}/*
#定义数据节点存放的路径
#要有足够的空间存放 ————非常重要
#chmod -R 777 ${HADOOP_DIR_DATA}
mkdir -p /home/data/hadoop/hdfs
mkdir -p ${HADOOP_DIR_DATA}/temp
mkdir -p ${HADOOP_DIR_DATA}/name
mkdir -p ${HADOOP_DIR_DATA}/data
chown -R hadoop.hadoop ${HADOOP_DIR_DATA}
chown -R hadoop.hadoop ${HADOOP_DIR_DATA}/*

# 配置hadoop用户的免密登录(以hadoop用户执行以下命令)
su - hadoop -c "mkdir -p ~/.ssh;cd ~/.ssh;ssh-keygen -t dsa -P '' -f id_dsa"

#######################	第二部分安装hadoop 	#######################
echo "第二部分安装hadoop"
## 重新设置下载链接
HADOOP_URL=$URL_FILE/hadoop
mkdir -p ${HADOOP_DIR_INSTALL}
## 如果文件不存在，则下载
if [ ! -f "$HADOOP_FILE" ]
then
    wget $HADOOP_URL/$HADOOP_FILE
fi
rm -rf $HADOOP_HOME
echo "解压中……"
tar -zxf $HADOOP_FILE -C ${HADOOP_DIR_INSTALL}

## 备份文件
mkdir -p /home/backup/$HADOOP_VERSION
cp -rf $HADOOP_DIR_CONF/* /home/backup/$HADOOP_VERSION
cp -rf ./$HADOOP_VERSION/* $HADOOP_DIR_CONF
## 替换变量
sed -i "s@\${JAVA_HOME}@${JAVA_HOME}@g" $HADOOP_DIR_CONF/hadoop-env.sh
mkdir -p ${HADOOP_PID_DIR};sed -i "s@\${HADOOP_PID_DIR}@${HADOOP_PID_DIR}@g" $HADOOP_DIR_CONF/hadoop-env.sh
sed -i "s@\${JAVA_HOME}@${JAVA_HOME}@g" $HADOOP_DIR_CONF/yarn-env.sh

## 修改core-site.xml配置文件
cat > $HADOOP_DIR_CONF/core-site.xml <<END
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
    <property>
        <name>fs.default.name</name>
        <value>hdfs://${HADOOP_MASTER}:9000</value>
    </property>
    <property>
        <name>io.file.buffer.size</name>
        <value>131072</value>
    </property>
    <property>
        <name>hadoop.tmp.dir</name>
        <value>${HADOOP_DIR_DATA}/temp</value>
         <description>A base for other temporary directories.</description>
    </property>
    <property>
        <name>hadoop.proxyuser.hduser.hosts</name>
        <value>*</value>
    </property>
        <property>
        <name>hadoop.proxyuser.hduser.groups</name>
        <value>*</value>
    </property>
</configuration>
END

## 修改hdfs-site.xml配置：
cat > $HADOOP_DIR_CONF/hdfs-site.xml <<END
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
    <property>
        <name>dfs.namenode.secondary.http-address</name>
        <value>${HADOOP_MASTER}:9001</value>
        <description></description>
    </property>
    <property>
        <name>dfs.namenode.name.dir</name>
        <value>file:${HADOOP_DIR_DATA}/name</value>
        <description></description>
    </property>
    <property>
        <name>dfs.datanode.data.dir</name>
        <value>file:${HADOOP_DIR_DATA}/data</value>
        <description></description>
    </property>
    <property>
        <name>dfs.replication</name>
        <value>2</value>
        <description>storage copy number</description>
    </property>
    <property>
        <name>dfs.webhdfs.enabled</name>
        <value>true</value>
        <description></description>
    </property>
    <property>
		<name>dfs.permissions</name>
		<value>false</value>
		<description>
			If "true", enable permission checking in HDFS.
			If "false", permission checking is turned off,
			but all other behavior is unchanged.
			Switching from one parameter value to the other does not change the mode,
			owner or group of files or directories.
		</description>
	</property>
	<property>
		<name>dfs.hosts.exclude</name>
		<value>$HADOOP_DIR_CONF/excludes</value>
	</property>
</configuration>
END

## 修改mapred-site.xml配置：
cat > $HADOOP_DIR_CONF/mapred-site.xml <<END
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
    <property>
        <name>mapreduce.framework.name</name>
        <value>yarn</value>
        <description></description>
    </property>
    <property>
        <name>mapreduce.jobhistory.address</name>
        <value>${HADOOP_MASTER}:10020</value>
        <description></description>
    </property>
    <property>
        <name>mapreduce.jobhistory.webapp.address</name>
        <value>${HADOOP_MASTER}:19888</value>
        <description></description>
    </property>
    <property>
		<name>dfs.hosts.exclude</name>
		<value>$HADOOP_DIR_CONF/excludes</value>
		<final>true</final>
	</property>
<!--
    <property>
        <name>mapred.job.tracker</name>
        <value>${HADOOP_MASTER}:9001</value>
        <description>JobTracker visit path</description>
    </property>
-->
</configuration>
END

## 修改yarn-site.xml配置：
cat > $HADOOP_DIR_CONF/yarn-site.xml <<END
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
<configuration>
    <property>
        <name>yarn.nodemanager.aux-services</name>
        <value>mapreduce_shuffle</value>
        <description></description>
    </property>
    <property>
        <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
        <value>org.apache.hadoop.mapred.ShuffleHandler</value>
        <description></description>
    </property>
    <property>
        <name>yarn.resourcemanager.address</name>
        <value>${HADOOP_MASTER}:8032</value>
        <description></description>
    </property>
    <property>
        <name>yarn.resourcemanager.scheduler.address</name>
        <value>${HADOOP_MASTER}:8030</value>
        <description></description>
    </property>
    <property>
        <name>yarn.resourcemanager.resource-tracker.address</name>
        <value>${HADOOP_MASTER}:8031</value>
        <description></description>
    </property>
    <property>
        <name>yarn.resourcemanager.admin.address</name>
        <value>${HADOOP_MASTER}:8033</value>
        <description></description>
    </property>
    <property>
        <name>yarn.resourcemanager.webapp.address</name>
        <value>${HADOOP_MASTER}:8088</value>
        <description></description>
    </property>
    <property>  
	    <name>yarn.log-aggregation-enable</name>  
	    <value>true</value>
	    <description>Aggregation is not enabled. Try the nodemanager ...</description>
	</property>
</configuration>
END

touch $HADOOP_DIR_CONF/excludes
# 再次确认修改一些文件权限（非常重要）
chown -R hadoop.hadoop ${HADOOP_DIR_OWN}
chown -R hadoop.hadoop ${HADOOP_DIR_OWN}/*
chown -R hadoop.hadoop ${HADOOP_DIR_DATA}
chown -R hadoop.hadoop ${HADOOP_DIR_DATA}/*