#!/bin/bash
set -e
##解决方案实例
##说明：搭建一个高性能高并发的视频分享和管理网站

##有N台相似的Linux服务器，无外网访问能力
##需要安装和实现以下软件或服务：
##jdk，Apache，tomcat，ftp，mysql，redis，nginx
##其中实现应用服务器负载均衡，mysql数据库主从同步并定期清理日志表，
##ngigx流媒体服务，redis缓存服，webapp批量部署和同步和文件备份，监控日志管理服务
## 搭建nginx静态服务器，提供文件下载服务

## 读取配置文件
. ../config/my-config.sh

## 创建公钥，建立信任关系(手动)
read -p "是否创建公钥，建立过的可以跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	ssh-keygen -t rsa
	for i in {1..3};do
		echo "${hosts[$i]} == ${users[$i]} == ${pwd[$i]}"
		ssh ${users[$i]}@${hosts[$i]} "mkdir -p ~/.ssh"
		cat ~/.ssh/id_rsa.pub | ssh ${users[$i]}@${hosts[$i]} 'cat >> ~/.ssh/authorized_keys'
	done
fi

## 1..开始安装
read -p "是否主机安装，安装的过的可以跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	## 1.0 提权
	chmod a+x /home/install -R
	## 1.1 删除rpm安装的软件
	cd /home/install/common;./uninstall-rpm.sh
	## 1.2 安装mysql
	cd /home/install/mysql;./installed-mysql.sh 1 master
	## 1.3 安装java
	cd /home/install/java;./installed-jdk.sh
	## 1.4 安装tomcat集群
	cd /home/install/tomcat;./single-installed.sh;./cluster-installed.sh
	## 1.5 安装apache
	cd /home/install/apache;./single-installed.sh;./balance-installed.sh
	## 1.6 安装nginx
	cd /home/install/nginx;./installed-nginx.sh
fi

read -p "是否安装从机，安装的过的可以跳过(Y/N):" isY
if [ "${isY}" == "y" ] || [ "${isY}" == "Y" ];then
	for i in {1..3};do
		count=$(($i+1))
	## 1.1 下载脚本，并删除已rpm安装的软件
		ssh ${users[$i]}@${hosts[$i]} "mkdir -p /home/install"
		## 下载脚本
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install;wget -N -r -nH -np -k -L -p $URL_SHELL"
		## 提权
		ssh ${users[$i]}@${hosts[$i]} "chmod a+x /home/install -R"
		## 删除rpm安装的软件
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install/common;./uninstall-rpm.sh"

	## 2.循环远程服务器执行jdk安装脚本
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install/java;./installed-jdk.sh"

	## 4.循环远程服务器执行tomcat负载均衡安装脚本
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install/tomcat;./single-installed.sh;./cluster-installed.sh"

	## 6.循环远程服务器执行mysql主从安装脚本
		ssh ${users[$i]}@${hosts[$i]} "cd /home/install/mysql;./installed-mysql.sh $count single"

	## 7.循环远程服务器执行webapp应用部署和文件同步脚本

	## 8.循环远程服务器执行监控日志管理脚本

	done
fi
