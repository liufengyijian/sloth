#!/bin/bash
set -e
## 检查输入参数
if [ $# -lt 1 ]; then
    echo "$0 错误：请传入参数，(1、端口号; 2、其他:8181)！"
	echo "例如：./$0 8080 haha或 ./$0 8888 xixi"
	exit 2
fi
## 读取配置文件
. ../config/my-config.sh

## 运行单机安装
./single-installed.sh

##4. 修改软件的相关配置文件，本地化或优化配置；
cp -f balance-$APACHE_CNF $APACHE_HOME/conf/httpd.conf
sed -i "s#@{port}#${1}#g" $APACHE_HOME/conf/httpd.conf
cp -f balance-uriworkermap.properties $APACHE_HOME/conf/uriworkermap.properties

cat > balance-workers.properties <<END
worker.list=loadBalanceServers, jk_watcher

END

tmpstr=""
for i in ${arrndx[@]};do
	count=$(($i+1))
	workername=s${count}
	tmpstr=${tmpstr},${workername}
	cat >> balance-workers.properties <<END
# server ${count}
# ------------------------
worker.${workername}.port=8${count}${count}9
worker.${workername}.host=${hosts[$i]}
worker.${workername}.type=ajp13
worker.${workername}.lbfactor=10
worker.${workername}.socket_timeout=300
worker.${workername}.connection_pool_size=800
worker.${workername}.connection_pool_minsize=25
worker.${workername}.connection_pool_timeout=600

END
done

cat >> balance-workers.properties <<END
worker.loadBalanceServers.type=lb
worker.loadBalanceServers.balance_workers=${tmpstr#*,}
worker.loadBalanceServers.sticky_session=false
worker.jk_watcher.type=status
worker.jk_watcher.mount=/admin/jk
worker.retries=3
END

cp -f balance-workers.properties $APACHE_HOME/conf/workers.properties

../common/change-iptables.sh ${1}
## 重启
service httpd restart