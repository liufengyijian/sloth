-- GRANT REPLICATION SLAVE ON *.* to '${mySyncUser}'@'%' identified by '${mySyncPwd}';  10.68.43.213
-- CHANGE MASTER TO MASTER_HOST='192.168.1.111', MASTER_PORT=13326, MASTER_USER='${mySyncUser}', MASTER_PASSWORD='${mySyncPwd}', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154, MASTER_CONNECT_RETRY=60;
STOP SLAVE;
CHANGE MASTER TO MASTER_HOST='10.68.43.212', MASTER_PORT=13326, MASTER_USER='${mySyncUser}', MASTER_PASSWORD='${mySyncPwd}', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154, MASTER_CONNECT_RETRY=60;
START SLAVE;