#!/bin/bash
set -e
## 读取配置文件
. ../config/my-config.sh
## 如果文件不存在，则下载
if [ ! -f "$JDK_FILE" ]
then
	#wget http://10.68.43.211:9300/mysql/mysql-5.7.12-1.el6.x86_64.rpm-bundle.tar
	#wget http://192.168.1.200:9300/mysql/mysql-5.7.12-linux-glibc2.5-x86_64.tar
    wget $DOWNLOAD_URL/$JDK_FILE
fi

if [ ! -d "$JAVA_HOME" ]
then
    echo "Make dir $JAVA_HOME!"
    mkdir -p "$JAVA_HOME"
fi
## 解压文件
mkdir $MYSQL_INSTALL_DIR
tar -zxvf $MYSQL_INSTALLED -C $MYSQL_INSTALL_DIR
#切换到安装目录
cd $MYSQL_INSTALL_DIR
#创建软连接（快捷方式）
ln -s $MYSQL_INSTALL_VERSION $MYSQL_VERSION
#切换当前目录
cd $MYSQL_VERSION
#新增用户组和用户
groupadd mysql
useradd -g mysql mysql
#改变当前目录的拥有者和用户组
chown -R mysql .
chgrp -R mysql .
#回到上级目录
cd ..
#复制服务文件到系统启动目录，并修改该权限
cp -f $MYSQL_INID /etc/init.d/mysqld
chmod a+x /etc/init.d/mysqld
chkconfig mysqld on
#复制控制文件到系统默认路径
cp -f $MYSQL_CNF /etc/my.cnf
#创建快捷方式到用户环境变量，保证mysql客户端可以快速使用
ln -s $MYSQL_INSTALL_DIR/$MYSQL_VERSION/bin/mysql /usr/bin
#启动服务
service mysqld start
## 添加防火墙规则
sh $HOME_INSTALL/change-iptables.sh $MYSQL_PORT
## 初始化数据库
mysql -P $MYSQL_PORT –u root –p -D mysql < $HOME_INSTALL/init-mysql.sql

##1.下载安装包，并上传到服务器；
wget http://localhost:8080/download/haha.tar.gz
##2. 解压安装包到指定目录，修改文件的读写执行权限；
tar -zxvf haha.tar.gz
##3. 编译源文件，并进行安装；
./configure 
make
make install
##4. 修改软件的相关配置文件，本地化或优化配置；
mv haha.cnf.default /cnf/haha.cnf
##5. 修改用户环境变量，或建立软链接，方便快速调用软件命令；
ln -s /home/bin/haha /usr/bin
##6. 把软件注册为服务，并随系统启动；
cp -f haha.service /etc/init.d/hahad
chmod a+x /etc/init.d/hahad
chkconfig hahad on
##7. 修改防火墙配置，以便外部访问；
../common/change-iptables.sh 8080
##8. 设置监控日志和备份目录，方便维护；
cp -f haha.cnf /home/backup/haha
##9. 能够安全干净删除已安装软件，并重新安装
echo 请编写卸载脚本！
